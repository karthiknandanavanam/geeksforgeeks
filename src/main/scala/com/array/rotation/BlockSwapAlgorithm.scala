package com.array.rotation

object BlockSwapAlgorithm {

  def rotate(arr: Array[Int], d: Int) = {
    val len = arr.length
    swap(arr, 0, d - 1, d, len - 1)
  }

  def swap(arr: Array[Int], s: Int, m: Int, mP: Int, e: Int): Unit = {
    val aSize = m - s + 1
    val bSize = e - mP + 1
    (aSize, bSize) match {
      case (a, b) if a < b => {
        val mid = e - aSize + 1
        swap(arr, s, m, mid, e)
        swap(arr, s, m, m + 1, mid - 1)
      }
      case (a, b) if a > b => {
        val mid = s + bSize - 1
        swap(arr, s, mid, mP, e)
        swap(arr, mid + 1, mP - 1, mP, e)
      }
      case (a, b) if a == b => {
        var l = s
        var r = mP
        while (l <= m) {
          val temp = arr(l)
          arr(l) = arr(r)
          arr(r) = temp
          l = l + 1
          r = r + 1
        }
      }
    }
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(2)
    val arr = inputs.next().split(" ").map(r => r.toInt)
    val d = inputs.next().toInt
    rotate(arr, d)
    println(arr.mkString(" "))
  }
}
