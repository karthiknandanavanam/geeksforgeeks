package com.array.rotation

object CyclicallyRotateClockwiseArrayByOne {

  def rotateClockwise(arr: Array[Int]) = {
    val len = arr.length
    val temp = arr(len - 1)
    for (i <- (len - 1) to 1 by -1) {
      arr(i) = arr(i - 1)
    }
    arr(0) = temp
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(1)
    val arr = inputs.next().split(" ").map(a => a.toInt)
    rotateClockwise(arr)
    println(arr.mkString(" "))
  }
}
