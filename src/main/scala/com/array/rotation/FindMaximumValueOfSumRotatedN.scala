package com.array.rotation

/*
1 2 3 4 5 6 7 8 9 10
 */
object FindMaximumValueOfSumRotatedN {

  def rotateSum(arr: Array[Int], j: Int, sum: Int, rSum: Int, max: Int): Int = {
    val len = arr.length
    j match {
      case a if a == len - 1 => max
      case 0 => val r = arr.zipWithIndex.map(r => r._1 * r._2).sum; rotateSum(arr, j + 1, sum, r, r)
      case _ => val r = rSum + sum - len * arr(len - j); rotateSum(arr, j + 1, sum, r, Math.max(r, max))
    }
  }

  def main(args: Array[String]) = {
    val arr = scala.io.Source.stdin.getLines().next().split(" ").map(r => r.toInt)
    val sum = arr.sum
    val res = rotateSum(arr, 0, sum, 0, Int.MinValue)
    println(res)
  }
}
