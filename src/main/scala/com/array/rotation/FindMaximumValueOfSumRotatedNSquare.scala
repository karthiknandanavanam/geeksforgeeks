package com.array.rotation

/*
1 2 3 4 5 6 7 8 9 10
 */
object FindMaximumValueOfSumRotatedNSquare {

  def rotate(arr: Array[Int]) = {
    val len = arr.length
    val temp = arr(0)
    (1 until len).foreach(r => arr(r - 1) = arr(r))
    arr(len - 1) = temp
  }

  def main(args: Array[String]) = {
    val arr = scala.io.Source.stdin.getLines().next().split(" ").map(r => r.toInt)
    val len = arr.length
    val res = (0 until len).map(_ => {
      rotate(arr)
      arr.zipWithIndex.map { case (a, b) => a * b }.sum
    }).max
    println(res)
  }
}
