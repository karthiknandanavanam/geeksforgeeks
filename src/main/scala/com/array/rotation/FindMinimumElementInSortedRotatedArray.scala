package com.array.rotation

/*
5 6 1 2 3 4
1 2 3 4
2 1

 */
object FindMinimumElementInSortedRotatedArray {

  def minimum(arr: Array[Int], l: Int, r: Int): Int = {
    val m = l + (r - l) / 2
    m match {
      case a if isPivot(arr, a) => arr(a)
      case a if a == l => minimum(arr, a + 1, r)
      case a if arr(a) > arr(r) => minimum(arr, a, r)
      case a if arr(a) < arr(r) => minimum(arr, l, a)
    }
  }

  def isPivot(arr: Array[Int], i: Int) = {
    if (i == 0 && arr(i) < arr(i + 1)) {
      true
    }
    else if (i != 0 && arr(i) < arr(i - 1)) {
      true
    }
    else {
      false
    }
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(3)

    for (input <- inputs) {
      val arr = input.split(" ").map(r => r.toInt)
      val res = minimum(arr, 0, arr.length - 1)
      println(res)
    }
  }
}
