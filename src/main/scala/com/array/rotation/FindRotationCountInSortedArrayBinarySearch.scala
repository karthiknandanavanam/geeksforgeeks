package com.array.rotation

/*
4 5 6 7 8 9 1 2 3
 */
object FindRotationCountInSortedArrayBinarySearch {

  def pivot(arr: Array[Int], l: Int, r: Int): Int = {
    val m = l + (r - l) / 2
    m match {
      case a if isPivot(arr, a) => a - 1
      case a if arr(a) > arr(r) => pivot(arr, a, r)
      case a if arr(a) > arr(r) => pivot(arr, l, a)
    }
  }

  def isPivot(arr: Array[Int], i: Int) = {
    if (arr(i) < arr(i - 1)) true else false
  }

  def main(args: Array[String]) = {
    val arr = scala.io.Source.stdin.getLines().next().split(" ").map(r => r.toInt)
    val p = pivot(arr, 0, arr.length - 1)
    println(p)
  }
}
