package com.array.rotation

/*
5 6 7 8 9 10 1 2 3 4
 */
object FindRotationCountInSortedArrayLinear {

  def pivot(arr: Array[Int]) = {
    val len = arr.length
    (1 until len).takeWhile(i => arr(i - 1) < arr(i)).last
  }

  def main(args: Array[String]) = {
    val arr = scala.io.Source.stdin.getLines().next().split(" ").map(r => r.toInt)
    val p = pivot(arr)
    println(p)
  }
}