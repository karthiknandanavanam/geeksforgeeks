package com.array.rotation

object JugglingAlgorithm {

  def rotate(arr: Array[Int], d: Int) = {
    val len = arr.length
    val sets = if (len % d == 0) len / d else 1
    val set = if (len % d == 0) d else 1

    for (i <- 0 until set) {
      val temp = arr(i)
      (set + i until len by set) foreach (j => arr(j - set) = arr(j))
      arr(i + (set * (sets - 1))) = temp
    }
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(2)
    val arr = inputs.next().split(" ").map(a => a.toInt)
    val d = inputs.next().toInt
    rotate(arr, d)
    println(arr.mkString(" "))
  }
}
