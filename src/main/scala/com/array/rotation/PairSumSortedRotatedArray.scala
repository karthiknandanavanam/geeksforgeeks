package com.array.rotation

/*
3 4 5 6 7 8 9 10 1 2
19
3 4 5 6 7 8 9 10 1 2
0
 */
object PairSumSortedRotatedArray {

  def pivot(arr: Array[Int]) = {
    val res = arr.zipWithIndex
      .filter(a => a._2 >= 1 && a._2 < arr.length)
      .find {case (_, i) => arr(i - 1) > arr(i)}
    if (res.isDefined) res.get._2 - 1 else arr.length - 1
  }

  def findPair(arr: Array[Int], s: Int, e: Int, sum: Int): (Int, Int) = {
    val len = arr.length
    var st = s
    var en = e
    while (st != en) {
      val value = arr(st) + arr(en)
      (value, sum) match {
        case (a, b) if a == b => return (arr(st), arr(en))
        case (a, b) if a < b => st = inc(st)
        case (a, b) if a > b => en = dec(en)
      }
    }

    def inc(i: Int) = if (i + 1 < len) i + 1 else 0
    def dec(i: Int) = if (i - 1 < 0) len - 1 else i - 1
    (0, 0)
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(2)
    val arr = inputs.next().split(" ").map(r => r.toInt)
    val sum = inputs.next().toInt

    val p = pivot(arr)
    val res = findPair(arr, p + 1, p, sum)
    println(res)
  }
}
