package com.array.rotation

/*
1 3 5 7 9
2
3
4
 */
object QuicklyFindMultipleLeftRotation {

  def main(args: Array[String]) = {
    val lines = scala.io.Source.stdin.getLines()
    val arr = lines.next().split(" ").map(r => r.toInt)
    val rotates = (1 to 3).map(_ => lines.next()).map(r => r.toInt)
    val lookup = arr ++ arr

    var pos = 0
    for(i <- rotates) {
      val len = arr.length
      pos = (pos + i) % len
      println((pos until pos + len).map(r => lookup(r)).mkString(" "))
    }
  }
}
