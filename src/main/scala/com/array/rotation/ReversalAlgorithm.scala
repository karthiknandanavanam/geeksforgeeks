package com.array.rotation

object ReversalAlgorithm {

  def rotate(arr: Array[Int], d: Int) = {
    val len = arr.length
    reverse(arr, 0, d - 1)
    reverse(arr, d, len - 1)
    reverse(arr, 0, len - 1)
  }

  def reverse(arr: Array[Int], s: Int, e: Int) = {
    var start = s
    var end = e
    while (start < end) {
      val temp = arr(start)
      arr(start) = arr(end)
      arr(end) = temp
      start = start + 1
      end = end - 1
    }
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(2)
    val arr = inputs.next().split(" ").map(a => a.toInt)
    val d = inputs.next().toInt
    rotate(arr, d)
    println(arr.mkString(" "))
  }
}
