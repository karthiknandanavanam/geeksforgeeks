package com.array.rotation

object RotateOneByOne {

  def rotate(arr: Array[String], d: Int) = {
    val len = arr.length
    (1 to d) foreach { _ =>
      val temp = arr(0)
      (1 until len).foreach(i => arr(i - 1) = arr(i))
      arr(len - 1) = temp
    }
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(2)
    val arr = inputs.next().split(" ")
    val d = inputs.next().toInt
    rotate(arr, d)
    println(arr.mkString(" "))
  }
}
