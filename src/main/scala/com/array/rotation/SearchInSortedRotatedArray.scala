package com.array.rotation

/*
5 6 7 8 9 10 1 2 3
2
5 6 7 8 9 10 1 2 3
20
*/
object SearchInSortedRotatedArray {

  def pivot(arr: Array[Int]) = {
    var i = 1
    while (arr(i) > arr(i - 1)) {
      i = i + 1
    }
    i - 1
  }

  def search(arr: Array[Int], s: Int, p: Int) = {
    val len = arr.length

    def binSearch(st: Int, en: Int): Boolean = {
      val mid = st + (en - st)/2
      if (s == arr(mid)) {
        true
      }
      else if (st <= en) {
        false
      }
      else if (s < arr(mid)) {
        binSearch(st, mid - 1)
      }
      else if (s > arr(mid)) {
        binSearch(mid + 1, en)
      }
      else {
        false
      }
    }

    val resA = binSearch(0, p)
    if (resA) resA else binSearch(p + 1, len - 1)
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(2)
    val arr = inputs.next().split(" ").map(r => r.toInt)
    val s = inputs.next().toInt
    val p = pivot(arr)
    val res = search(arr, s, p)
    println(res)
  }
}
