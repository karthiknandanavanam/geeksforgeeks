package com.array.rotation

object UseTempArray {

  def rotate(arr: Array[Int], d: Int) = {
    val temp = (0 until d).map(i => arr(i))
    var i = 0
    for (j <- d until arr.size)  {
      arr(i) = arr(j)
      i = i + 1
    }

    i = 0
    for (j <- arr.size - d until arr.size) {
      arr(j) = temp(i)
      i = i + 1
    }
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(2)
    val arr = inputs.next().split(" ").map(a => a.toInt)
    val d = inputs.next().toInt
    rotate(arr, d)
    println(arr.mkString(" "))
  }
}
