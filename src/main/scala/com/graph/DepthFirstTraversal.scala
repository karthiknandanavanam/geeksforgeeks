package com.graph

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/*
4
6
0 1
0 2
1 2
2 0
2 3
3 3
*/
object DepthFirstTraversal {

  case class Graph(v: Int) {
    val edges = new Array[ListBuffer[Int]](v).map(_ => new ListBuffer[Int]())

    def addEdge(verA: Int, verB: Int) = {
      edges(verA) += verB
    }

    def display = println(edges.map(r => r.mkString(" ")).mkString("\n"))

    def dfs(s: Int) = {
      val done = new Array[Boolean](v)
      val stack = mutable.Stack[Int]()
      stack.push(s)

      val res = new ListBuffer[Int]()
      while (stack.nonEmpty) {
        val vertex = stack.pop()
        res += vertex
        done(vertex) = true
        edges(vertex).filter(r => !done(r)).map(r => stack.push(r))
      }
      res
    }
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val vertices = scan.nextInt()
    val edges = scan.nextInt()

    val graph = Graph(vertices)
    (0 until edges).map(_ => (scan.nextInt(), scan.nextInt())).map(r => graph.addEdge(r._1, r._2))

    print(graph.dfs(2).mkString(" "))
  }
}
