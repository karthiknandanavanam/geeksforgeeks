package com.graph

import scala.collection.mutable

/*
4 6
0 1
0 2
1 2
2 0
2 3
3 3

6
6
5 2
5 0
4 0
4 1
2 3
3 1
 */
object DetectACycleInDirectedGraph {

  case class Graph(v: Int) {
    val edges = new Array[mutable.ListBuffer[Int]](v).map(_ => new mutable.ListBuffer[Int]())

    def addEdge(a: Int, b: Int) =  edges(a) += b

    def isCycle: Boolean = {
      val done = mutable.Set[Int]()
      val nodes = mutable.Queue[Int]()
      (0 until edges.length).foreach(r => nodes.enqueue(r))

      while (nodes.nonEmpty) {
        val traverse = nodes.dequeue()
        done.add(traverse)
        val cycleDone = mutable.Set[Int](traverse)
        val queue = mutable.Queue(traverse)
        while (queue.nonEmpty) {
          val node = queue.dequeue()
          val childNodes = edges(node)
          if (childNodes.exists(r => cycleDone.contains(r))) {
            return true
          }
          childNodes.foreach(r => queue.enqueue(r))
          cycleDone.add(node)
        }
      }
      false
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val vertices = scan.nextInt()
    val edges = scan.nextInt()

    val graph = Graph(vertices)
    (0 until edges).map(_ => (scan.nextInt(), scan.nextInt()))
      .foreach(r => graph.addEdge(r._1, r._2))

    println(graph.isCycle)
  }
}
