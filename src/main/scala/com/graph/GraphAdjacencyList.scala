package com.graph

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/*
5
6
0 1
0 4
1 2
1 3
1 4
2 3
 */
object GraphAdjacencyList {

  case class Graph(v: Int) {
    val list = new Array[mutable.ListBuffer[Int]](v).map(_ => new ListBuffer[Int]())

    def addEdge(verA: Int, verB: Int) = {
      list(verA) += verB
      list(verB) += verA
    }

    def display = println(list.map(r => r.mkString(" ")).mkString("\n"))
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val vertices = scan.nextInt()
    val edges = scan.nextInt()

    val graph = Graph(vertices)
    (0 until edges).map(_ => (scan.nextInt(), scan.nextInt())).foreach(r => graph.addEdge(r._1, r._2))
    graph.display
  }
}
