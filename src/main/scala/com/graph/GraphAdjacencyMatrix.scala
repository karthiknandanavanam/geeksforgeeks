package com.graph

/*
5
6
0 1
0 4
1 2
1 3
1 4
2 3
 */
object GraphAdjacencyMatrix {

  case class Graph(v: Int) {
    val mat = Array.ofDim[Int](v, v)

    def addEdge(verA: Int, verB: Int) = {
      mat(verA)(verB) = 1
      mat(verB)(verA) = 1
    }

    def display = println(mat.map(r => r.mkString(" ")).mkString("\n"))
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val vertices = scan.nextInt()
    val edges = scan.nextInt()

    val graph = Graph(vertices)
    (0 until edges).map(_ => (scan.nextInt(), scan.nextInt())).foreach(r => graph.addEdge(r._1, r._2))

    graph.display
  }
}
