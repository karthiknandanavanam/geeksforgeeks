package com.graph

import scala.collection.mutable

/*
6
6
5 2
5 0
4 0
4 1
2 3
3 1
 */
object TopologicalSorting {

  case class Graph(v: Int) {
    val edges = new Array[mutable.ListBuffer[Int]](v).map(_ => new mutable.ListBuffer[Int]())

    def addEdge(a: Int, b: Int) = {
      edges(a) += b
    }

    def display = {
      edges.map(r => r.mkString(" ")).mkString("\n")
    }

    def topologicalSorting = {
      val sort = mutable.Stack[Int]()

      val done = new Array[Boolean](v)
      val stack = mutable.Stack[Int](0)
      done(0) = true
      while (stack.nonEmpty) {
        val vertex = stack.head
        edges(vertex).filter(r => !done(r)).foreach { r =>
          stack.push(r)
          done(r) = true
        }
        if (vertex == stack.head) {
          sort.push(stack.pop())
          if (stack.isEmpty) {
            val remaining = done.zipWithIndex.filter(r => !r._1)
            if (remaining.nonEmpty) {
              stack.push(remaining.head._2)
              done(remaining.head._2) = true
            }
          }
        }
      }

      sort.mkString(" ")
    }
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val vertices = scan.nextInt()
    val edges = scan.nextInt()

    val graph = Graph(vertices)
    (0 until edges).map(_ => (scan.nextInt(), scan.nextInt())).foreach { r =>
      graph.addEdge(r._1, r._2)
    }

    println(graph.display)
    println(graph.topologicalSorting)
  }
}