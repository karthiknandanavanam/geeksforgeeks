package com.matrix

import scala.collection.mutable

/*
3 4
1 0 0 1
0 0 1 0
0 0 0 0

2 3
0 0 0
0 0 1
 */
object ABooleanMatrix {

  def solve(mat: Array[Array[Int]], m: Int, n: Int) = {
    val rows = mutable.Set[Int]()
    val columns = mutable.Set[Int]()
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        if (mat(i)(j) == 1) {
          rows.add(i)
          columns.add(j)
        }
      }
    }

    for (i <- rows) {
      for (j <- 0 until n) {
        mat(i)(j) = 1
      }
    }

    for (j <- columns) {
      for (i <- 0 until m) {
        mat(i)(j) = 1
      }
    }
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val m = scan.nextInt()
    val n = scan.nextInt()

    val mat = Array.ofDim[Int](m, n)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        mat(i)(j) = scan.nextInt()
      }
    }

    solve(mat, m, n)
    println(mat.map(r => r.mkString(" ")).mkString("\n"))
  }
}
