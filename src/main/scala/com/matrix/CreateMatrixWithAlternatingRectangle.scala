package com.matrix

object CreateMatrixWithAlternatingRectangle {

  def construct(m: Int, n: Int) = {

    val mat = Array.ofDim[Char](m, n)
    var i = 0
    while (i < Math.ceil(m) - 1) {
      val value = if (i % 2 == 0) 'X' else '0'

      for (j <- i until n - i) {
        mat(i)(j) = value
      }

      for (p <- i until m - i) {
        mat(p)(n - i - 1) = value
      }

      for (j <- n - i - 1 to i by -1) {
        mat(m - i - 1)(j) = value
      }

      for (p <- m - i - 1 to i by -1) {
        mat(p)(i) = value
      }

      i = i + 1
    }
    mat
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val m = scan.nextInt()
    val n = scan.nextInt()

    val res = construct(m, n)
    println(res.map(r => r.mkString(" ")).mkString("\n"))
  }
}
