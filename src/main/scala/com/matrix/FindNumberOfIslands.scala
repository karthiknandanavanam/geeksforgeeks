package com.matrix

/*
5 5
1 1 0 0 0
0 1 0 0 1
1 0 0 1 1
0 0 0 0 0
1 0 1 0 1
 */
object FindNumberOfIslands {

  def numberOfIslands(mat: Array[Array[Int]], m: Int, n: Int) = {

    def dfs(i: Int, j: Int): Unit = {
      (i, j) match {
        case (a, b) if a < 0 || b < 0 =>
        case (a, b) if a >= m || b >= n =>
        case (a, b) if mat(a)(b) == 0 =>
        case (a, b) if mat(a)(b) == 1 => {
          mat(a)(b) = 0
          dfs(a - 1, b)
          dfs(a - 1, b - 1)
          dfs(a - 1, b + 1)
          dfs(a, b + 1)
          dfs(a + 1, b)
          dfs(a + 1, b - 1)
          dfs(a + 1, b + 1)
        }
      }
    }

    var count = 0
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        if (mat(i)(j) == 1) {
          count = count + 1
          dfs(i, j)
        }
      }
    }

    count
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val m = scan.nextInt()
    val n = scan.nextInt()

    val mat = Array.ofDim[Int](m, n)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        mat(i)(j) = scan.nextInt()
      }
    }

    val res = numberOfIslands(mat, m, n)
    println(res)
  }
}
