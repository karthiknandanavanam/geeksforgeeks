package com.matrix

/*
4 4
0 1 1 1
0 0 1 1
1 1 1 1
0 0 0 0
 */
object FindRowWithMaximumNumberOfOnes {

  def findMaxOne(mat: Array[Array[Int]], m: Int, n: Int) = {
    var i = 0
    var j = n - 1
    var index = Int.MaxValue
    var min = Int.MaxValue
    while (i < m) {
      while (j >= 0 && mat(i)(j) == 1) j = j - 1
      if (j < min) {
        index = i
        min = j
      }
      i = i + 1
    }
    index
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val m = scan.nextInt()
    val n = scan.nextInt()

    val mat = Array.ofDim[Int](m, n)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        mat(i)(j) = scan.nextInt()
      }
    }

    val res = findMaxOne(mat, m, n)
    println(res)
  }
}
