package com.matrix

/*
6 5
0 1 1 0 1
1 1 0 1 0
0 1 1 1 0
1 1 1 1 0
1 1 1 1 1
0 0 0 0 0
 */
object MaximumSizeOneSquareMatrix {

  def maximumSquare(mat: Array[Array[Int]], m: Int, n: Int) = {

    def value(i: Int, j: Int) = {
      (i, j) match {
        case (a, b) if a < 0 || b < 0 => 0
        case (a, b) => mat(a)(b)
      }
    }

    var max = -1
    var pos = (-1, -1)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        val (cA, cB) = (i - 1, j - 1)
        val (tA, tB) = (i - 1, j)
        val (lA, lB) = (i, j - 1)

        val min = Array(value(cA, cB), value(tA, tB), value(lA, lB)).min
        mat(i)(j) = if (mat(i)(j) == 0) 0 else mat(i)(j) + min
        if (mat(i)(j) > max) {
          max = mat(i)(j)
          pos = (i, j)
        }
      }
    }
    println(mat.map(r => r.mkString(" ")).mkString("\n"))

    println((pos._1 - max + 1, pos._2 - max + 1, pos._1, pos._2))
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val m = scan.nextInt()
    val n = scan.nextInt()

    val mat = Array.ofDim[Int](m, n)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        mat(i)(j) = scan.nextInt()
      }
    }

    maximumSquare(mat, m, n)
  }
}
