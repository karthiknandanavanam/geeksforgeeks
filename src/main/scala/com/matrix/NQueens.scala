package com.matrix

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object NQueens {

  def nQueens(m: Int) = {
    val mat = Array.ofDim[Int](m, m)

    val picks = mutable.ListBuffer[(Int, Int)]()
    find(mat, 0, picks)
    mat
  }

  def find(mat: Array[Array[Int]], row: Int, picks: mutable.ListBuffer[(Int, Int)]): Boolean = {
    if (row == mat.length) {
      true
    }
    else {
      for (i <- 0 until mat.length) {
        val pick = (row, i)
        if (check(picks, pick)) {
          mat(row)(i) = 1
          picks += pick
          val res = find(mat, row + 1, picks)
          if (!res) {
            picks.remove(picks.length - 1)
            mat(row)(i) = 0
          }
          else {
            return true
          }
        }
      }
      false
    }
  }

  def check(picks: ListBuffer[(Int, Int)], newPick: (Int, Int)) = {
    !picks.exists(r => r._1 == newPick._1 || r._2 == newPick._2 || Math.abs(r._1 - newPick._1) == Math.abs(r._2 - newPick._2))
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val m = scan.nextInt()
    val res = nQueens(m)
    println(res.map(r => r.mkString(" ")).mkString("\n"))
  }
}
