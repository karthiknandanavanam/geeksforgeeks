package com.matrix

/*
4 4
1 2 3 4
5 6 7 8
9 10 11 12
13 14 15 16

6 6
1 2 3 4 5 6
1 2 3 4 5 6
1 2 3 4 5 6
1 2 3 4 5 6
1 2 3 4 5 6
1 2 3 4 5 6
 */
object PrintMatrixInSpiralForm {

  def printSpiral(mat: Array[Array[Int]], m: Int, n: Int) = {
    val res = new Array[Int](m * n)
    var index = 0
    for (p <- 0 until m / 2) {
      for (i <- p until n - p) {
        res(index) = mat(p)(i)
        index = index + 1
      }
      for (i <- p + 1 until m - p) {
        res(index) = mat(i)(n - 1 - p)
        index = index + 1
      }
      for (i <- n - 1 - p - 1 to p by -1) {
        res(index) = mat(m - 1 - p)(i)
        index = index + 1
      }
      for (i <- m - 1 - p - 1 to p + 1 by -1) {
        res(index) = mat(i)(p)
        index = index + 1
      }
    }
    println(res.mkString(" "))
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val m = scan.nextInt()
    val n = scan.nextInt()

    val mat = Array.ofDim[Int](m, n)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        mat(i)(j) = scan.nextInt()
      }
    }

    printSpiral(mat, m, n)
  }
}
