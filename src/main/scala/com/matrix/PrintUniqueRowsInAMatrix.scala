package com.matrix

/*
5 5
1 0 1 1 0
0 1 0 0 1
1 1 1 0 0
1 0 1 1 0
1 1 1 0 0
 */
object PrintUniqueRowsInAMatrix {

  trait Trie {
    def isEmpty = this match {
      case Empty => true
      case _: NonEmpty => false
    }

    def nonEmpty = !isEmpty

    def hasOne = this match {
      case a: NonEmpty if a.one.nonEmpty => true
      case _ => false
    }

    def hasZero = this match {
      case a: NonEmpty if a.zero.nonEmpty => true
      case _ => false
    }

    def contains(arr: Array[Int]): Boolean = {
      (arr, this) match {
        case (a, _) if a.isEmpty => true
        case (_, b) if b.isEmpty => false
        case (a, b: NonEmpty) if a.head == 1 && this.hasOne => b.one.contains(arr.tail)
        case (a, b: NonEmpty) if a.head == 1 && !this.hasOne => false
        case (a, b: NonEmpty) if a.head == 0 && this.hasZero => b.zero.contains(arr.tail)
        case (a, b: NonEmpty) if a.head == 0 && !this.hasZero => false
      }
    }

    def insert(arr: Array[Int]): Unit = {
      (arr, this) match {
        case (a, _) if a.isEmpty =>
        case (a, b: NonEmpty) if a.head == 1 && b.one.isEmpty => b.one = NonEmpty(1); b.one.insert(arr.tail)
        case (a, b: NonEmpty) if a.head == 1 && b.one.nonEmpty => b.one.insert(arr.tail)
        case (a, b: NonEmpty) if a.head == 0 && b.zero.isEmpty => b.zero = NonEmpty(0); b.zero.insert(arr.tail)
        case (a, b: NonEmpty) if a.head == 0 && b.zero.nonEmpty => b.zero.insert(arr.tail)
      }
    }
  }

  case class NonEmpty(a: Int) extends Trie {
    var zero: Trie = Empty
    var one: Trie = Empty
  }

  case object Empty extends Trie {
  }

  def printUniqueRows(mat: Array[Array[Int]], m: Int, n: Int) = {
    val trie = NonEmpty(-1)

    for (i <- 0 until m) {
      val arr = mat(i)
      if (!trie.contains(arr)) {
        println(arr.mkString(" "))
        trie.insert(arr)
      }
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val m = scan.nextInt()
    val n = scan.nextInt()

    val mat = Array.ofDim[Int](m, n)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        mat(i)(j) = scan.nextInt()
      }
    }

    printUniqueRows(mat, m, n)
  }
}
