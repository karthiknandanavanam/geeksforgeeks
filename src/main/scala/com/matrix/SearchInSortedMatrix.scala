package com.matrix

/*
4 4 29
10 20 30 40
15 25 35 45
27 29 37 48
32 33 39 50
 */
object SearchInSortedMatrix {

  def find(mat: Array[Array[Int]], m: Int, n: Int, f: Int): (Int, Int) = {
    var i = 0
    var j = 0
    while (i < m && j < n) {
      (i, j, f) match {
        case (a, b, c) if mat(a)(b) == c => return (i, j)
        case (a, b, c) if a + 1 < m && b + 1 < n && mat(a + 1)(b + 1) <= c => i = i + 1; j = j + 1
        case (a, b, c) if a + 1 < m && mat(a + 1)(b) <= c => i = i + 1
        case (a, b, c) if b + 1 < n && mat(a)(b + 1) <= c => j = j + 1
        case _ => return (-1, -1)
      }
    }
    (-1, -1)
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val m = scan.nextInt()
    val n = scan.nextInt()
    val f = scan.nextInt()
    val mat = Array.ofDim[Int](m, n)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        mat(i)(j) = scan.nextInt()
      }
    }

    val res = find(mat, m, n, f)
    println(res)
  }
}
