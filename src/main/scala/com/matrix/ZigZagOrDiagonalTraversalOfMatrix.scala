package com.matrix

/*
5 4
1     2     3     4
5     6     7     8
9    10    11    12
13   14    15    16
17   18    19    20
 */
object ZigZagOrDiagonalTraversalOfMatrix {

  def printDiagonal(mat: Array[Array[Int]], m: Int, n: Int) = {
    for (p <- 0 until m) {
      var i = p
      var j = 0
      while (i >= 0 && j < n) {
        print(mat(i)(j))
        print(" ")
        j = j + 1
        i = i - 1
      }
      println()
    }

    for (p <- 1 until n) {
      var i = m - 1
      var j = p
      while (i >= 0 && j < n) {
        print(mat(i)(j))
        print(" ")
        j = j + 1
        i = i - 1
      }
      println()
    }
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val m = scan.nextInt()
    val n = scan.nextInt()

    val mat = Array.ofDim[Int](m, n)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        mat(i)(j) = scan.nextInt()
      }
    }

    printDiagonal(mat, m, n)
  }
}
