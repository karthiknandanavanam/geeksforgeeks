package com.stack

import scala.collection.mutable

/*
7
1 2
1 3
1 4
2
1 5
1 6
2
 */
object QueueWithStacks {

  class Queue {
    val stackEn = mutable.Stack[Int]()
    val stackDe = mutable.Stack[Int]()

    def enqueue(a: Int) = stackEn.push(a)
    def dequeue: Int = {
      if (stackDe.nonEmpty) {
        stackDe.pop()
      }
      else {
        while (stackEn.nonEmpty) stackDe.push(stackEn.pop())
        stackDe.pop()
      }
    }
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextLine().toInt

    val q = new Queue()
    (0 until size).map(_ => scan.nextLine()).map(r => r.split(" ")).foreach {
      case a if a.length == 2 && a(0).toInt == 1 => val res = a(1).toInt; q.enqueue(res);
      case a if a.length == 1 => val res = q.dequeue; println(res)
    }
  }
}
