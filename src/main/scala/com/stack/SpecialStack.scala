package com.stack

import scala.collection.mutable

/*
5
18
19
29
15
16
 */
object SpecialStack {

  class StackSpecial {
    val stack = mutable.Stack[Int]()
    val min = mutable.Stack[Int]()

    def push(a: Int) = {
      stack.push(a)
      if (min.nonEmpty && min.head < a) {
        min.push(min.head)
      }
      else {
        min.push(a)
      }
      println(s"(item, min) = (${stack.head}, ${min.head})")
    }

    def pop = {
      val minimum = min.pop()
      val item = stack.pop()
      println(s"(item, min) = (${item}, ${minimum})")
      item
    }
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)

    val stack = new StackSpecial()
    val size = scan.nextInt()
    (0 until size).map(_ => scan.nextInt()).foreach(r => stack.push(r))
  }
}
