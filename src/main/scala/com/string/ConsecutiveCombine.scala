package com.string

object ConsecutiveCombine {

  def combine(str: Array[Char]) = {
    var i = 0
    var j = 0
    while (i < str.length) {
      val c = str(i)
      var count = 0
      while (i < str.length && c == str(i)) {
        i = i + 1
        count = count + 1
      }
      if (count == 1) {
        j = j + 1
      }
      else {
        str(j) = c
        str(j + 1) = count.toString.charAt(0)
        j = j + 2
      }
    }
    str.take(j).mkString("")
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val arr = scan.next().toCharArray
    val res = combine(arr)
    println(res)
  }
}
