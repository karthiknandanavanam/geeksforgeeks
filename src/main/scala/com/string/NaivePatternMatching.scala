package com.string

import scala.collection.mutable

/*
AABAACAADAABAAABAA
AABA
 */
object NaivePatternMatching {

  def patternMatch(input: String, matcher: String) = {
    val found = mutable.ListBuffer[Int]()
    (input, matcher) match {
      case (a, b) if b.length > a.length => found
      case (a, b) => {
        var i = 0
        while (i < a.length) {
          if (isMatch(i, a, b)) {
            found += i
          }
          i = i + 1
        }
        found
      }
    }
  }

  def isMatch(s: Int, a: String, b: String): Boolean = {
    if (b.length <= a.length - s) {
      var j = 0
      while (j < b.length) {
        if (a(s + j) != b(j)) {
          return false
        }
        j = j + 1
      }
      true
    }
    else {
      false
    }
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val input = scan.next()
    val matcher = scan.next()

    val res = patternMatch(input, matcher)
    println(res.mkString(" "))
  }
}
