package com.tree.binary

/*
DBEAFC
ABDECF
 */
object ConstructTreeInorderAndPreorder {

  trait Tree {

    def data = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.d
    }

    def isEmpty = this match {
      case NonEmpty(d) => false
      case Empty => true
    }

    def addLeft(t: Tree) = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.l = t
    }

    def addRight(t: Tree) = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.r = t
    }

    def left = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.l
    }

    def right = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.r
    }

    def inorder: String = this match {
      case Empty => ""
      case a: NonEmpty => a.left.inorder + a.d + a.right.inorder
    }
  }

  case class NonEmpty(d: Char) extends Tree {
    var l: Tree = Empty
    var r: Tree = Empty
  }

  case object Empty extends Tree {
  }

  object Tree {

    def findLeft(pre: Array[Char], leftInorder: Array[Char]) = {
      val leftTree = leftInorder.toList.toSet
      pre.takeWhile(r => leftTree.contains(r)).length
    }

    def construct(inorder: Array[Char], preorder: Array[Char]): Tree = {
      (inorder, preorder) match {
        case (i, p) if i.isEmpty && p.isEmpty => Empty
        case (i, p) => {
          val n = NonEmpty(p.head)
          val leftInorder = inorder.subSequence(0, i.indexOf(n.data)).toString.toCharArray
          val rightInorder = inorder.subSequence(i.indexOf(n.data) + 1, inorder.length).toString.toCharArray
          val pre = p.tail
          val index = findLeft(pre, leftInorder)
          val leftPreorder = pre.subSequence(0, index).toString.toCharArray
          val rightPreorder = pre.subSequence(index, pre.length).toString.toCharArray
          n.addLeft(construct(leftInorder, leftPreorder))
          n.addRight(construct(rightInorder, rightPreorder))
          n
        }
      }
    }
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val inorder = scan.next().toCharArray
    val preorder = scan.next().toCharArray
    val res = Tree.construct(inorder, preorder)
    println(res.inorder)
  }
}
