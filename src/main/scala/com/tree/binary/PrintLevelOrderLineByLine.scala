package com.tree.binary

import scala.collection.mutable

/*
7
1 2 3 4 5 6 7
 */
object PrintLevelOrderLineByLine {
  trait Tree {
    def isEmpty = this match {
      case Empty => true
      case NonEmpty(a) => false
    }

    def nonEmpty = !isEmpty

    def data = this match {
      case Empty => throw new UnsupportedOperationException()
      case NonEmpty(a) => a
    }

    def left = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.l
    }

    def right = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.r
    }

    def setLeft(t: Tree) = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.l = t
    }

    def setRight(t: Tree) = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.r = t
    }

    def insert(t: Int) = this match {
      case Empty => NonEmpty(t)
      case a: NonEmpty if a.left.isEmpty => a.setLeft(NonEmpty(t))
      case a: NonEmpty if a.right.isEmpty => a.setRight(NonEmpty(t))
    }

    def levelorder = {
      val level = new mutable.Queue[Tree]()
      level.enqueue(this)
      val list = new mutable.ListBuffer[mutable.ListBuffer[Int]]()
      while (level.nonEmpty) {
        val q = level.clone()
        level.clear()

        val levelList = new mutable.ListBuffer[Int]()
        while (q.nonEmpty) {
          val node = q.dequeue()
          levelList += node.data
          if (node.left.nonEmpty) level.enqueue(node.left)
          if (node.right.nonEmpty) level.enqueue(node.right)
        }
        list += levelList
      }
      list
    }
  }

  case class NonEmpty(d: Int) extends Tree {
    var l: Tree = Empty
    var r: Tree = Empty
  }

  case object Empty extends Tree {
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    var nodes = (0 until size).map(_ => scan.nextInt()).map(r => NonEmpty(r)).toList

    val root = nodes.head
    val q = new mutable.Queue[Tree]()
    q.enqueue(nodes.head)
    nodes = nodes.tail
    while (nodes.nonEmpty) {
      if (q.head.left.nonEmpty && q.head.right.nonEmpty) q.dequeue()

      val parent = q.head
      val node = nodes.head

      if (parent.left.isEmpty) parent.setLeft(node)
      else if (parent.right.isEmpty) parent.setRight(node)
      q.enqueue(node)
      nodes = nodes.tail
    }

    println(root.levelorder.map(m => m.mkString(" ")).mkString("\n"))
  }
}
