package com.tree.binary

import sun.invoke.empty.Empty

import scala.collection.mutable

/*
7
1 2 3 4 5 6 7
 */

object TreeTraversals {

  trait Tree {
    def data: Int = this match {
      case a: NonEmpty => a.data
      case a: Empty => throw new UnsupportedOperationException()
    }

    def isEmpty = this match {
      case NonEmpty(a) => false
      case Empty => true
    }

    def nonEmpty = !isEmpty

    def addLeft(t: Tree) = this match {
      case a: NonEmpty => a.l = t
      case a: Empty => throw new UnsupportedOperationException()
    }

    def addRight(t: Tree) = this match {
      case a: NonEmpty => a.r = t
      case a: Empty => throw new UnsupportedOperationException()
    }

    def left = this match {
      case a: NonEmpty => a.l
      case a: Empty => throw new UnsupportedOperationException()
    }

    def right = this match {
      case a: NonEmpty => a.r
      case a: Empty => throw new UnsupportedOperationException()
    }

    def insert(t: Int) = this match {
      case Empty => NonEmpty(t)
      case a: NonEmpty if a.l.isEmpty => val node = NonEmpty(t); a.addLeft(node); a
      case a: NonEmpty if a.r.isEmpty => val node = NonEmpty(t); a.addRight(node); a
    }

    def inorder: String = this match {
      case Empty => ""
      case a: NonEmpty => a.left.inorder + a.data + " " + a.right.inorder
    }

    def preorder: String = this match {
      case Empty => ""
      case a: NonEmpty => a.data + " " + a.left.preorder + a.right.preorder
    }

    def postorder: String = this match {
      case Empty => ""
      case a: NonEmpty => a.left.postorder + a.right.postorder + " " + a.data
    }
  }

  case class NonEmpty(d: Int) extends Tree {
    var l: Tree = Empty
    var r: Tree = Empty
    override def data: Int = d
  }

  case object Empty extends Tree {
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    var nodes = (0 until size).map(_ => scan.nextInt()).map(r => NonEmpty(r)).toList

    val root = nodes.head
    val q = new mutable.Queue[Tree]()
    q.enqueue(nodes.head)
    nodes = nodes.tail
    while (nodes.nonEmpty) {
      if (q.head.left.nonEmpty && q.head.right.nonEmpty) q.dequeue()
      val node = nodes.head
      val parent = q.head
      if (parent.left.isEmpty) parent.addLeft(node)
      else if (parent.right.isEmpty) parent.addRight(node)
      q.enqueue(node)
      nodes = nodes.tail
    }

    println(root.preorder)
    println(root.inorder)
    println(root.postorder)
  }
}
