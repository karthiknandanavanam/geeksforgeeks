package com.tree.heap

import scala.collection.mutable

/*
7
40 50 100 40 15 30 10
 */
object BinaryHeap {

  class Heap {
    var arr = mutable.ListBuffer[Int]()

    def parent(i: Int) = i match {
      case 0 => throw new UnsupportedOperationException()
      case i => Math.ceil(i / 2.0).toInt - 1
    }

    def left(i: Int) = i * 2 + 1

    def right(i: Int) = i * 2 + 2

    def nonEmpty = {
      if (arr.nonEmpty && arr.head != Int.MaxValue) {
        true
      }
      else {
        arr.clear()
        false
      }
    }

    def heapify(i: Int): Unit = i match {
      case 0 =>
      case a => {
        val par = parent(a)
        if (arr(par) > arr(a)) {
          swap(par, a)
          heapify(par)
        }
      }
    }

    def swap(i: Int, j: Int): Unit = {
      val temp = arr(i)
      arr(i) = arr(j)
      arr(j) = temp
    }

    def insert(i: Int) = {
      arr = arr ++ List(i)
      heapify(arr.length - 1)
    }

    def delete(i: Int) = {
      arr(i) = Int.MinValue
      heapify(i)
      extractMin
    }

    def extractMin = arr match {
      case a if a.isEmpty => throw new UnsupportedOperationException()
      case a => val res = a.head; arr(0) = Int.MaxValue; heapifyRev(0); res
    }

    def display = arr.mkString(" ")

    def heapifyRev(i: Int): Unit = {
      i match {
        case a if left(a) >= arr.length =>
        case a if left(a) < arr.length && right(a) >= arr.length && arr(left(a)) < arr(a) => swap(a, left(a)); heapifyRev(left(a))
        case a if left(a) < arr.length && right(a) < arr.length && arr(left(a)) < arr(right(a)) => swap(a, left(a)); heapifyRev(left(a))
        case a if left(a) < arr.length && right(a) < arr.length && arr(left(a)) > arr(right(a)) => swap(a, right(a)); heapifyRev(right(a))
        case a if left(a) < arr.length && right(a) < arr.length && arr(left(a)) == arr(right(a)) => swap(a, left(a)); heapifyRev(left(a))
      }
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val inputs = (0 until size).map(_ => scan.nextInt())

    val heap = new Heap()
    for (i <- inputs) {
      heap.insert(i)
      println(heap.display)
    }

    heap.delete(5)
    println(heap.display)

    while (heap.nonEmpty) {
      val res = heap.extractMin
      println(res)
    }
  }
}
